# GawdServer 1.0.0.0


### Server Versions

| Server | Version | Download |
| ------ | :-----: | -------- |
| Minecraft | 1.16.5 | [Versions](https://mcversions.net/) |
| PaperSpigot | 575 | [Versions](https://papermc.io/downloads#Paper-1.16) |
| Waterfall | 403 | [Versions](https://papermc.io/downloads#Waterfall) |
| Geyser | 658 | [Versions](https://ci.opencollab.dev/job/GeyserMC/job/Geyser/job/master/) |


### Plugin Versions (Bukkit)

| Plugin | Version | Download |
| ------ | :-----: | -------- |
| AsyncWorldEdit | 3.8.2 | [Versions](https://www.spigotmc.org/resources/asyncworldedit.327/history) |
| Dynmap | 3.1 GS | [Versions](https://www.spigotmc.org/resources/dynmap.274/history) |
| EssentialsX | 2.18.2 | [Versions](https://www.spigotmc.org/resources/essentialsx.9089/history) |
| Floodgate | 4 | [Versions](https://ci.opencollab.dev/job/GeyserMC/job/Floodgate/job/master/) |
| GawdChat | 1.0 Beta | N/A |
| LockettePro | 2.10.10 | [Versions](https://www.spigotmc.org/resources/lockettepro.74354/history) |
| LogBlock | 1.16.1.1 | [Versions](https://www.spigotmc.org/resources/logblock.67333/history) |
| Multiverse-Core | 4.2.2 | [Versions](https://dev.bukkit.org/projects/multiverse-core/files) |
| Multiverse-Inventories | 4.2.2 | [Versions](https://dev.bukkit.org/projects/multiverse-inventories/files) |
| Multiverse-NetherPortals | 4.2.1 | [Versions](https://dev.bukkit.org/projects/multiverse-netherportals/files) |
| Multiverse-Portals | 4.2.1 | [Versions](https://dev.bukkit.org/projects/multiverse-portals/files) |
| Multiverse-SignPortals | 4.2.0 | [Versions](https://dev.bukkit.org/projects/multiverse-signportals/files) |
| NuVotifier | 2.7.2 | [Versions](https://www.spigotmc.org/resources/nuvotifier.13449/) |
| ProtocolLib | 4.6.0 | [Versions](https://www.spigotmc.org/resources/protocollib.1997/history) |
| Vault | 1.7.3 | [Versions](https://www.spigotmc.org/resources/vault.34315/history) |
| WorldEdit | 7.2.4 | [Versions](https://dev.bukkit.org/projects/worldedit/files) |
| WorldGuard | 7.0.4 | [Versions](https://dev.bukkit.org/projects/worldguard/files) |
| zPermissions | 1.5 | [Versions](https://www.spigotmc.org/resources/zpermissions.11736/) |


### Plugin Versions (BungeeCord)

| Plugin | Version | Download |
| ------ | :-----: | -------- |
| Floodgate | 4 | [Versions](https://ci.opencollab.dev/job/GeyserMC/job/Floodgate/job/master/) |
| GlobalTabList | 1.12 | [Versions](https://www.spigotmc.org/resources/globaltablist.1117/history) |
| HoverPlayerList | 1.0 | [Versions](https://www.spigotmc.org/resources/hoverplayerlist.12189/history) |
| Yamler | 2.4.0 | [Versions](https://www.spigotmc.org/resources/yamler.315/history) |
